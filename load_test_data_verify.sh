#!/bin/bash
POD_CONF_FILE="podman.conf.sh"
. scriptspod/$POD_CONF_FILE

# data loaded
study_id="PRJNA330606"
metadata_file="PRJNA330606_Wang_1_sample_metadata.csv"
annotation_tool="mixcr"
output_dir=".tmp"
url="http://localhost:80"
 
# call verify_dataload with 6 arguments
# 1 - study_id
# 2 - study_dir (directory with study metadata and rearrangement data files)
# 3 - metadata_file
# 4 - annotation tool (file format for the rearrangement files mixcr, vquest, or airr)
# 5 - output_dir (for report files)
# 6 - url (base url of service to query)
scriptspod/verify_dataload.sh \
     $study_id \
     test_data \
     $metadata_file \
     $annotation_tool \
     .tmp \
	 $url

echo "---------- done call to verify"
ret_val="0"

check_report () 
{
    file=$1
    echo "----- check: $1"
    i=-1
    while IFS= read -r line
    do
        echo "[${i}]$line"
        pat='^\d+,'
        #echo "${i},"
        if [[ $line = ${i},*  ]]; then
            #echo "-------------------- report result: $line"
            IFS="," read -a myarray <<< $line   # 16 elements
            #myarray=(`echo $line | tr ',' ' '`) # 15 elements
            #echo "My array: ${myarray[@]}"
            arr_size=${#myarray[@]}
            report_result=${myarray[${arr_size}-1]}
            if [ $report_result != "True"  ]; then
                ret_val="-1"
            fi
            echo "report_result: ${report_result}"
        fi
        ((i=i+1))
    done < "${file}"

}

search_dir=$output_dir
for entry in "$search_dir"/*
do
    #echo "$entry"
    if [[ $entry == *"${study_id}_Facet_Count_curator_count_Annotation_count_"*  ]]; then
        check_report "$entry"
    fi
done

echo "---------- done check report (ret_val=${ret_val})"
exit $ret_val

