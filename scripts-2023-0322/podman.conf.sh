#!/bin/bash
# -----------------------------------------------------------------------------
# steps:
# (1) provide tcp external port of pod (POD_EX_PORT)
# (2) provide a name for the pod
# (3) provide user id for the mongodb process which will access the host filesystem
# (4) mv podman.conf.sh.EDIT podman.conf.sh
# -----------------------------------------------------------------------------


#if (( $EUID != 0 )); then
    #echo "Please run with sudo.."
    #exit
#fi

# service admin e-mail
sysadmin_email="noreply@example.com";

# hostname 
host_name=$(hostname);

# path to backup directory, shared by all pods on host
# this pod will have its own dir inside that path 
#PATH_BKUP_DIR="/var/data/bkup";
PATH_BKUP_DIR="${RUNNER_TEMP_PROJECT_DIR}/bkup";

# if set to 1, bkup_file_rotation.sh will only use backup.daily folder
# by default, files are kept for 14 days in that folder, see script for details
BKUP_USES_ONLY_DIR_DAILY="1";

#echo inside podman.conf.sh

# if you run production and master branch of turnkey on same machine:
# (1) provide container name suffix 
# (2) adjust different external port for api access and db access (if required)
# (3) use different POD_NAME_SVC value like turnkey-service-dev

# container name suffix, leave empty single installation of production
DATABASE_CONT_NAME_SUFFIX="-v4-2023-0322";
API_CONT_NAME_SUFFIX="-v4-2023-0322";
DATALOADING_CONT_NAME_SUFFIX="-v4-2023-0322";
PERFORMANCE_TESTING_CONT_NAME_SUFFIX="-v4-2023-0322";

# container tags are configured for production installation
# for dev installations use other value

# prod:turnkey-v3 dev:master
DATABASE_TAG="turnkey-v4"
# prod:turnkey-v3 dev:latest
API_TAG="turnkey-v4"
# prod:turnkey-v3-dataloading dev:turnkey-dataloading-latest, turnkey-v4-dataloading
DATALOADING_TAG="turnkey-v4"
# prod:turnkey-v3-performance dev:turnkey-performance-latest
PERFORMANCE_TESTING_TAG="turnkey-performance-latest"

# podman command name
PODMAN_CMD="podman";
#echo PODMAN_CMD = $PODMAN_CMD 

# pod external port
POD_EX_PORT="8443";
#echo POD_EX_PORT = $POD_EX_PORT

# https port for apache, use (POD_EX_PORT + 20)
POD_EX_PORT_HTTPS="8463"

# if specified, apache default https configuration will be replaced with that file
# see e.g. https://httpd.apache.org/docs/trunk/ssl/ssl_howto.html
HTTPS_CUSTOM_CONF_FULL_PATH="${RUNNER_TEMP_PROJECT_DIR}/scriptspod/000-default.conf"
# HTTPS_CUSTOM_CONF_FULL_PATH=""

# use apache https config, if true, self signed certificates are created in .ssl folder  
CERT_HTTPS_ENABLED_SELFSIGNED="FALSE"

# if true, provide full paths to custom certificates
CERT_HTTPS_ENABLED_CUSTOM="FALSE"

# full path to server certificate 
CERT_HTTPS_FULL_PATH_CERT="/var/data/scitest.cert.pem"

# full path to private key file
CERT_HTTPS_FULL_PATH_CERTKEY="/var/data/scitest.key.pem"

# full path to certificate chain
CERT_HTTPS_FULL_PATH_CERTCHAIN="/var/data/certchain.pem"

# specify full name of repository host running https
HTTPS_HOSTNAME=$host_name

# external port of mongo db process
POD_EX_PORT_DB="27017";

# pod service name
POD_NAME_SVC="turnkey-service-v4-2023-0322";
#echo POD_NAME_SVC = $POD_NAME_SVC

# service account id running the pod 
# legolas amann 1000,  d130-svr2 sysadm 1002
POD_USER_ID="986"

# existing file will trigger db backup
# created by load_ and update_ scripts
DO_BKUP_FLAG=${PATH_BKUP_DIR}/${POD_NAME_SVC}/BKUP_DB.FLAG

# 2023-0322
AIRR_INFO_TITLE="iReceptor Service"

# 2023-0322
AIRR_INFO_CONTACT_NAME="iReceptor"

# 2023-0322
AIRR_INFO_CONTACT_URL="http://ireceptor.org"

# 2023-0322
AIRR_INFO_CONTACT_EMAIL="support@ireceptor.org"

# 2023-0322
MAPPING_URL="https://raw.githubusercontent.com/sfu-ireceptor/config/turnkey-v4/AIRR-iReceptorMapping.txt"

# dns name of repository for access from inside pod (e.g. dataloading -> repo)
REPOSITORY_DNS_NAME=$host_name

# IP addr for that name
REPOSITORY_IPV4="10.0.0.149"

# TCP Port coul be different than POD_EX_PORT, if access via reverse proxy
REPOSITORY_PORT=$POD_EX_PORT

REPOSITORY_ACCESS=""
# if empty, dont add port
if [ -z "REPOSITORY_PORT"  ]
then
	REPOSITORY_ACCESS=$REPOSITORY_DNS_NAME
else
	REPOSITORY_ACCESS=$REPOSITORY_DNS_NAME:$REPOSITORY_PORT
fi

# override for stating
REPOSITORY_ACCESS="localhost:${REPOSITORY_PORT}"

# only used in development 
# HO vpn0 Home enp0s25 (you cannot use a device with an ip from 10/24
#  net because that is the podman network!!!)
# you cannot use lo or 127.0.0.1 here because that ip represents
# something different from within a pod
# eno1 vpn0 eth0
IP_LAN_DEVICE="eth0"

# -----------------------------------------------------------------------------

SCRIPT_DIR=`dirname "$0"`

CURRENTUSER=$(who | awk 'NR==1{print $1}')
#echo $CURRENTUSER

function wait () {
    arg1=$1
    arg2=$2
    secs=$arg2
    #secs=$((5 * 60))
    while [ $secs -gt 0 ]; do
        echo -ne "wait $arg2 secs (${arg1}) ... $secs\033[0K\r"
        sleep 1
        : $((secs--))
    done
}

LOGFILE_NAME="scriptspod_${host_name}_${POD_NAME_SVC}_.log"
if [ ! -e $SCRIPT_DIR/$LOGFILE_NAME  ];then
	touch $SCRIPT_DIR/$LOGFILE_NAME
	#chown $CURRENTUSER:$CURRENTUSER $SCRIPT_DIR/$LOGFILE_NAME
fi

function log () {
	local msg=$1
	echo $msg
	local dt=`date +%F_%T`
	echo "$dt $msg" >> $SCRIPT_DIR/$LOGFILE_NAME
}

function myip () {
    #IP_LAN_DEVICE="vpn0" see above
    current_IPv4_Lan=$(ip -o -4 addr list $IP_LAN_DEVICE | awk '{print $4}' | cut -d/ -f1)
    echo $current_IPv4_Lan
}

REPOSITORY_IPV4=$(myip)
log "---"

