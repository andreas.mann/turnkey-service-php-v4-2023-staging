#!/bin/bash
# Use this script inside the dataloading-mongo container
# location outside: .tmp inside /scratch

# data from outside script
# study_id=PRJNA330606
# study_dir=test_data
# metadata_file=PRJNA330606_Wang_1_sample_metadata.csv
# annotation_tool=mixcr
# output_dir=.tmp
# url=http://legolas:8443

# https://github.com/sfu-ireceptor/dataloading-mongo/blob/master/verify/joint_sanity_testing.sh
# $1 base_url       String containing URL to API server (e.g. https://airr-api2.ireceptor.org)
# $2 entry_point    Options: string 'rearragement' or string 'repertoire'
# $3 path_to_json   Enter full path to JSON directory where facet JSON query files will be stored
# $4 no_filters     Enter full path to JSON query nofilters
# $5 study_id       Enter study_id
# $6 mapping_file    Indicate the full path to where the mapping file is found
# $1 base_url        String containing URL to API server (e.g. https://airr-api2.ireceptor.org)
# $2 entry_point     Options: string 'rearragement' or string 'repertoire'
# $4 no_filters      Enter full path to JSON query nofilters
# $7 master_md       Full path to master metadata
# $5 study_id        Study ID (study_id) associated to this study
# $3 path_to_json    Enter full path to JSON directory where facet JSON query files will be stored
# $8 annotation_dir  Enter full path to where annotation files associated with study_id
# $9 details_dir     Enter full path where you'd like to store content feedback in CSV format
# $10 annotation_tool Enter the name of the tool used to process sequence data. Choices: igblast, vquest, mixcr

# read parameter from file
. /scratch/VERIFY_DATALOAD_CALLER_PARAMS.sh 

SCRIPT_DIR=`dirname "$0"`

cp /app/verify/nofilters.json /study/nofilters.json

chmod +x /app/verify/joint_sanity_testing.sh

# caller values from verify_dataload.sh
#         sh -c 'bash /app/verify/joint_sanity_testing.sh \
#                              ${url} \ 
#                              "repertoire" \
#                              /app/verify/facet_queries_for_sanity_tests/ \
#          /app/verify/nofilters.json \
#          ${study_id} \
#          /app/config/AIRR-iReceptorMapping.txt \
#          /study/${metadata_file} \
#          /study/ \

echo "please wait ..."

output=$(bash /app/verify/joint_sanity_testing.sh ${url} repertoire /scratch/ /study/nofilters.json ${study_id} /app/config/AIRR-iReceptorMapping.txt /study/${metadata_file} /study/ /output/ ${annotation_tool})

echo "caller done."
echo "$output"

